import pyperclip
import time
import json


def clear_clipboard_history():
    with open("../clipboard_history.json", "w") as file:
        json.dump([], file)


clear_clipboard_history()


def load_clipboard_history():
    try:
        with open("../clipboard_history.json", "r") as file:
            return json.load(file)
    except FileNotFoundError:
        return []

# Function to save clipboard history to a JSON file


def save_clipboard_history():
    with open("../clipboard_history.json", "w") as file:
        json.dump(clipboard_history, file)


# Initialize clipboard history by loading it from the JSON file
clipboard_history = load_clipboard_history()

# Get the initial clipboard content
previous_clipboard = pyperclip.paste()

# Function to check for new clipboard content and add it to the history


def check_clipboard():
    global previous_clipboard
    current_clipboard = pyperclip.paste()

    if current_clipboard != previous_clipboard:
        clipboard_history.append(current_clipboard)
        save_clipboard_history()  # Save the updated history to the JSON file
        previous_clipboard = current_clipboard


# Main loop to continuously check for clipboard changes
while True:
    check_clipboard()
    time.sleep(0.1)  # Adjust the sleep duration as needed (in seconds)
