import json

# Load the JSON file
with open('../clipboard_history.json', 'r') as file:
    input_data = json.load(file)

# Initialize empty lists for nodes and links
nodes = []
links = []
route_name = "walter_to_area1_"
ride_type = "jeep"

# Process each coordinate string and create nodes
for index, coordinate_str in enumerate(input_data):
    # Split the coordinate string into latitude and longitude
    lat, lon = map(float, coordinate_str.split(', '))

    # Create a node dictionary
    node = {
        "id": f"{route_name}{index}",
        "x": lat,
        "y": lon,
        "type": ride_type
    }

    # Append the node to the nodes list
    nodes.append(node)

    if index > 0:
        link = {
            "source": f"{route_name}{index - 1}",
            "target": f"{route_name}{index}",
            "weight": 1
        }
        links.append(link)

# Create the final JSON structure
output_data = {
    "nodes": nodes,
    "links": links
}

# Write the output JSON to a file
with open('../temporary_nodelist.json', 'w') as output_file:
    json.dump(output_data, output_file, indent=4)

print("Conversion completed. Output saved to 'temporary_nodelist.json'.")
