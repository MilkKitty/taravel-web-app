import os
import json


def load_master_nodelist():
    with open("../master_nodelist.json", "r") as file:
        return json.load(file)


def save_master_nodelist(data):
    with open("../master_nodelist.json", "w") as file:
        json.dump(data, file, indent=4)


master_nodelist = load_master_nodelist()
nodes = master_nodelist["nodes"]
links = master_nodelist["links"]

folders = ["../transport_routes", "../transport_routes/tricycle"]

for folder_path in folders:
    for filename in os.listdir(folder_path):
        if filename.endswith(".geojson"):
            filepath = os.path.join(folder_path, filename)

            with open(filepath, 'r') as file:
                geojson_data = json.load(file)

            route_name = os.path.splitext(filename)[0].lower()
            ride_type = "jeep" if folder_path == "../transport_routes" else "tricycle"

            for feature in geojson_data["features"]:
                if feature["geometry"]["type"] == "LineString":
                    coordinates = feature["geometry"]["coordinates"]
                    for index, coord in enumerate(coordinates):
                        lon, lat = coord[:2]
                        node = {
                            "id": f"{route_name}_{index}",
                            "x": lat,
                            "y": lon,
                            "type": ride_type
                        }
                        nodes.append(node)

                        if index > 0:
                            link = {
                                "source": f"{route_name}_{index - 1}",
                                "target": f"{route_name}_{index}",
                                "weight": 1,
                                "type": ride_type
                            }
                            links.append(link)

master_nodelist["nodes"] = nodes
master_nodelist["links"] = links
save_master_nodelist(master_nodelist)
